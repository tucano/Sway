#!/bin/sh
yay -Syu brightnessctl librewolf-bin alsa-utils pulsemixer vim foot mako wf-recorder wl-clipboard wlsunset \
sway swaybg swayidle swaylock sway-systemd swaytools swayhide grim slurp ttf-jetbrains-mono \
adobe-source-han-sans-jp-fonts noto-fonts-emoji xdg-desktop-portal-wlr rofi xorg-xwayland lite-xl-bin \
thunar tumbler mupdf file-roller p7zip wine nordic-darker-theme gvfs gvfs-mtp gvfs-smb  \
nordzy-cursors nordzy-icon-theme imv cmus mpv yt-dlp playerctl xdg-utils xdg-user-dirs ffmpegthumbnailer &&

xdg-user-dirs-update --force
cp -r .local ~/
cp -r .config/ ~/
cp .bash* ~/
cp .wp ~/

chmod +x ~/.local/bin/*
